'use strict';

const { src, dest, series, parallel, watch } = require('gulp');
const scss = require('gulp-sass');
const gulpif = require('gulp-if');
const autoprefixer = require('gulp-autoprefixer');
const uglifycss = require('gulp-uglifycss');
const spritesmith = require('gulp.spritesmith');
const merge = require('merge-stream');
const imagemin = require('gulp-imagemin');
const buffer = require('vinyl-buffer');
const concat = require('gulp-concat');

const isDev = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

const spriteVersion = 'v6';
const spriteCatVersion = 'v4';

function buildSprite() {
    const spriteData = src('./images/sprite/icons/*.png')
        .pipe(spritesmith({
            imgName: `sprite-${spriteVersion}.png`,
            imgPath: `../../images/sprite-${spriteVersion}.png`,
            cssName: 'sprite.less',
            // cssTemplate: 'images/sprite/icons.css.handlebars',
            retinaSrcFilter: ['images/sprite/icons/*@2x.png'],
            retinaImgName: `sprite-${spriteVersion}@2x.png`,
            retinaImgPath: `../../images/sprite-${spriteVersion}@2x.png`,
            cssOpts: {
                cssSelector: function (sprite) {
                    return '.' + sprite.name;
                }
            }
        }));

    const imgStream = spriteData.img
        .pipe(buffer())
        .pipe(imagemin())
        .pipe(dest('./images/'));

    const cssStream = spriteData.css
        .pipe(dest('./less/sprites/'));

    return merge(imgStream, cssStream);
}

function buildSpriteCategories() {
    const spriteData = src('./images/sprite/category-icons/*.png')
        .pipe(spritesmith({
            imgName: `sprite-categories-${spriteCatVersion}.png`,
            imgPath: `../../images/sprite-categories-${spriteCatVersion}.png`,
            cssName: 'sprite-categories.css',
            // cssTemplate: 'images/sprite/category-icons.css.handlebars',
            retinaSrcFilter: ['images/sprite/category-icons/*@2x.png'],
            retinaImgName: `sprite-categories-${spriteCatVersion}@2x.png`,
            retinaImgPath: `../../images/sprite-categories-${spriteCatVersion}@2x.png`,
            cssOpts: {
                cssSelector: function (sprite) {
                    return '.' + sprite.name;
                }
            },
        }));

    const imgStream = spriteData.img
        .pipe(buffer())
        .pipe(imagemin())
        .pipe(dest('./images/'));

    const cssStream = spriteData.css
        .pipe(dest('./less/sprites'));

    return merge(imgStream, cssStream);
}

function buildCss() {

    const scssStream = src('./scss/base.scss')
        .pipe(scss());

    return scssStream
        .pipe(concat('base.css'))
        .pipe(gulpif(!isDev, uglifycss()))
        .pipe(dest('./dist/css'));
}

function buildVendors() {
    return src([
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/bootstrap/dist/js/bootstrap.js',
        // './node_modules/selectize.js/dist/js/standalone/selectize.min.js',
        // './node_modules/owl.carousel/dist/owl.carousel.min.js',
        // './node_modules/jQuery-menu-aim/jquery.menu-aim.js',
        // './node_modules/autosize/dist/autosize.js',
        // './node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js'
    ])
        .pipe(dest('./dist/js/vendors/'));
}

function watcher() {
    watch('./scss/*.scss', { usePolling: true }, buildCss);
}

exports.sprite = buildSprite;

exports.spriteCategories = buildSpriteCategories;

exports.vendors = buildVendors;

exports.watch = series(buildCss, watcher);

exports.build = buildCss;
