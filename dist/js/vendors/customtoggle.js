const customToggle = {
  init() {
    this.toggleButton = document.querySelector('#toggle-button');
    this.fogLayer = document.querySelector('#navbar-fog');
    this.navBar = document.querySelector('#navbar-slider');
    this.bind(this.toggleButton, this.navBar, this.fogLayer, this.movement);
  },
  movement: { action: '', status: 'static' },
  bind(toggleButton, navBar, fogLayer, movement) {
    navbarToggle = function(action) {
      //console.log(this);
      if (action == 'show') {
        return function(e) {
          movement.action = 'show';
          fogLayer.classList.add('show');
          fogLayer.classList.add('fade');
        };
      } else if (action == 'hide') {
        return function(e) {
          movement.action = 'hide';
          navBar.classList.remove('show');
        };
      }
    };

    fogLayer.addEventListener(
      'transitionend',
      function() {
        movement.action == 'show' && navBar.classList.add('show');
        movement.action == 'hide' && fogLayer.classList.remove('show');
      }.bind(this)
    );

    navBar.addEventListener(
      'transitionend',
      function() {
        movement.action == 'hide' && fogLayer.classList.remove('fade');
      }.bind(this)
    );

    fogLayer.addEventListener('click', navbarToggle('hide').bind(this));
    toggleButton.addEventListener('click', navbarToggle('show').bind(this));
  }
};
customToggle.init();
